require 'rails_helper'

RSpec.describe CompaniesController, type: :controller do
   include Devise::Test::ControllerHelpers

   before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:owner]
    @current_owner = FactoryGirl.create(:owner)
    sign_in @current_owner
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do

    context "company exists" do
      context "Owner created company" do
        it "returns http success" do
          company = create(:company, owner: @current_owner)
          get :show, params: {id: company.id}
          expect(response).to have_http_status(:success)
        end
      end
    end
  end

  describe "POST #create" do
    before(:each) do
      @company_attributes = attributes_for(:company, owner: @current_owner)
      post :create, params: {company: @company_attributes}
    end

    it "redirect to new team" do
      expect(response).to have_http_status(302)
    end

    it "Create team with right attributes" do
      expect(Company.last.owner).to eql(@current_owner)
      expect(Company.last.name).to eql(@company_attributes[:name])
    end
  end

  describe "GET #edit" do

    it "returns http success" do
      company = create(:company, owner: @current_owner)
      get :edit, params: {id: company.id}
      expect(response).to have_http_status(:success)
    end
  end


  describe "GET #destroy" do
    it "returns http success" do
      company = create(:company, owner: @current_owner)
      delete :destroy, params: {id: company.id}
      expect(response).to redirect_to('/companies')
    end
  end

end
