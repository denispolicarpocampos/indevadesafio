require 'rails_helper'

RSpec.describe SalesmenController, type: :controller do
   include Devise::Test::ControllerHelpers

   before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:owner]
    @current_owner = FactoryGirl.create(:owner)
    sign_in @current_owner
    @current_company = FactoryGirl.create(:company, owner: @current_owner)
  end

  describe "GET #index" do
    it "returns http success" do
      get :index, params: {:company_id => @current_company.id}
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      salesman = FactoryGirl.create(:salesman, company: @current_company)
      get :show, params: {:company_id => @current_company.id, id: salesman.id}
      expect(response).to have_http_status(302)
    end
  end

  describe "POST #create" do
    before(:each) do
      @salesman = FactoryGirl.create(:salesman, company: @current_company)
      post :create, params: {:company_id => @current_company.id, salesman: { name: @salesman.name, company_id: @current_company.id } }
    end

    it "redirect to new team" do
      expect(response).to have_http_status(302)
    end

    it "Create team with right attributes" do
      expect(Salesman.last.company).to eql(@current_company)
      expect(Salesman.last.name).to eql(@salesman.name)
    end
  end

  describe "GET #edit" do

    it "returns http success" do
      salesman = create(:salesman, company: @current_company)
      get :edit, params: {:company_id => @current_company.id, id: salesman.id}
      expect(response).to have_http_status(302)
    end
  end


  describe "GET #destroy" do
    it "returns http success" do
      salesman = create(:salesman, company: @current_company)
      delete :destroy, params: {:company_id => @current_company.id, id: salesman.id}
      expect(response).to have_http_status(302)
    end
  end

end
