require 'rails_helper'

RSpec.describe GoalsController, type: :controller do
  include Devise::Test::ControllerHelpers

  before(:each) do
   @request.env["devise.mapping"] = Devise.mappings[:owner]
   @current_owner = FactoryGirl.create(:owner)
   sign_in @current_owner
   @current_company = FactoryGirl.create(:company, owner: @current_owner)
 end

 describe "GET #index" do
   it "returns http success" do
     get :index, params: {:company_id => @current_company.id}
     expect(response).to have_http_status(:success)
   end
 end

 describe "GET #show" do
   it "returns http success" do
     goal = create(:goal, company: @current_company)
     get :show, params: {:company_id => @current_company.id, id: goal.id}
     expect(response).to have_http_status(:success)
   end
 end

 describe "POST #create" do
   before(:each) do
     @goal = FactoryGirl.create(:goal, company: @current_company)
     post :create, params: {:company_id => @current_company.id, goal: { name: @goal.name, totalvalue: @goal.totalvalue, date_start: @goal.date_start, date_end: @goal.date_end, company_id: @current_company.id } }
   end

   it "redirect to new team" do
     expect(response).to have_http_status(302)
   end

   it "Create team with right attributes" do
     expect(Goal.last.company).to eql(@current_company)
     expect(Goal.last.name).to eql(@goal.name)
     expect(Goal.last.totalvalue).to eql(@goal.totalvalue)
     expect(Goal.last.date_start).to eql(@goal.date_start)
     expect(Goal.last.date_end).to eql(@goal.date_end)
   end
 end

 describe "GET #edit" do

   it "returns http success" do
     goal = create(:goal, company: @current_company)
     get :edit, params: {:company_id => @current_company.id, id: goal.id}
     expect(response).to have_http_status(:success)
   end
 end


 describe "GET #destroy" do
   it "returns http success" do
     goal = create(:goal, company: @current_company)
     delete :destroy, params: {:company_id => @current_company.id, id: goal.id}
     expect(response).to have_http_status(:success)
   end
 end

end
