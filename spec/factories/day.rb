FactoryGirl.define do
 factory :day do
   date_day
   value { rand(0..100) }
   goal
 end
end
