FactoryGirl.define do
 factory :company do
   name {FFaker::Name.name}
   owner
 end
end
