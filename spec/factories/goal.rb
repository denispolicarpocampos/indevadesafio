FactoryGirl.define do
 factory :goal do
   name { FFaker::Company.name }
   company
   date_start { FFaker::Time.date  }
   date_end { FFaker::Time.date   }
   totalvalue { rand(0..100) }
   month { FFaker::Time.month }
 end
end
