class CreateGoalSalesmen < ActiveRecord::Migration[5.0]
  def change
    create_table :goal_salesmen do |t|
      t.references :goal, foreign_key: true
      t.references :salesman, foreign_key: true

      t.timestamps
    end
  end
end
