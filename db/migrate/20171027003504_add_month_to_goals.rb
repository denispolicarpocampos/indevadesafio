class AddMonthToGoals < ActiveRecord::Migration[5.0]
  def change
    add_column :goals, :month, :date
  end
end
