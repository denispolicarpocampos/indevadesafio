class RemoveMonthFromGoals < ActiveRecord::Migration[5.0]
  def change
    remove_column :goals, :month, :integer
  end
end
