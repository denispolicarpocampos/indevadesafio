class CreateDaySalesmen < ActiveRecord::Migration[5.0]
  def change
    create_table :day_salesmen do |t|
      t.references :day, foreign_key: true
      t.references :salesman, foreign_key: true

      t.timestamps
    end
  end
end
