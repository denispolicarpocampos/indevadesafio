class CreateGoals < ActiveRecord::Migration[5.0]
  def change
    create_table :goals do |t|
      t.string :name
      t.references :company, foreign_key: true
      t.integer :month
      t.date :date_start
      t.date :date_end
      t.decimal :totalvalue

      t.timestamps
    end
  end
end
