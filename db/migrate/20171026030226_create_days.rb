class CreateDays < ActiveRecord::Migration[5.0]
  def change
    create_table :days do |t|
      t.date :date_day
      t.decimal :value
      t.references :goal, foreign_key: true

      t.timestamps
    end
  end
end
