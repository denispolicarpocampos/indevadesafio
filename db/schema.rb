# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171027003504) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.integer  "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id"], name: "index_companies_on_owner_id", using: :btree
  end

  create_table "day_salesmen", force: :cascade do |t|
    t.integer  "day_id"
    t.integer  "salesman_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["day_id"], name: "index_day_salesmen_on_day_id", using: :btree
    t.index ["salesman_id"], name: "index_day_salesmen_on_salesman_id", using: :btree
  end

  create_table "days", force: :cascade do |t|
    t.date     "date_day"
    t.decimal  "value"
    t.integer  "goal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["goal_id"], name: "index_days_on_goal_id", using: :btree
  end

  create_table "goal_salesmen", force: :cascade do |t|
    t.integer  "goal_id"
    t.integer  "salesman_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["goal_id"], name: "index_goal_salesmen_on_goal_id", using: :btree
    t.index ["salesman_id"], name: "index_goal_salesmen_on_salesman_id", using: :btree
  end

  create_table "goals", force: :cascade do |t|
    t.string   "name"
    t.integer  "company_id"
    t.date     "date_start"
    t.date     "date_end"
    t.decimal  "totalvalue"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date     "month"
    t.index ["company_id"], name: "index_goals_on_company_id", using: :btree
  end

  create_table "owners", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "name",                                null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_owners_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_owners_on_reset_password_token", unique: true, using: :btree
  end

  create_table "salesmen", force: :cascade do |t|
    t.string   "name"
    t.integer  "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_salesmen_on_company_id", using: :btree
  end

  add_foreign_key "companies", "owners"
  add_foreign_key "day_salesmen", "days"
  add_foreign_key "day_salesmen", "salesmen"
  add_foreign_key "days", "goals"
  add_foreign_key "goal_salesmen", "goals"
  add_foreign_key "goal_salesmen", "salesmen"
  add_foreign_key "goals", "companies"
  add_foreign_key "salesmen", "companies"
end
