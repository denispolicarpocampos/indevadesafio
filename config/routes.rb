Rails.application.routes.draw do
  root to: 'companies#index'
  resources :companies do
    resources :salesmen, only: [:index, :destroy, :create]
    resources :goals, only: [:new, :destroy, :create, :show] do
      resources :days, only: [:show, :edit, :update]
    end
  end
  devise_for :owners, :controllers => { registrations: 'registrations' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
