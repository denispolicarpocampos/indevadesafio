class GoalSalesman < ApplicationRecord
  belongs_to :goal
  belongs_to :salesman
end
