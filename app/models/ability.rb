class Ability
  include CanCan::Ability

  def initialize(owner)
    if owner
      can [:read, :destroy, :update], Company do |c|
        c.owner_id == owner.id
      end
    end
  end
end
