class Company < ApplicationRecord
  belongs_to :owner
  has_many :goal, dependent: :destroy
  has_many :salesman, dependent: :destroy
end
