class DaySalesman < ApplicationRecord
  belongs_to :day
  belongs_to :salesman

  accepts_nested_attributes_for :salesman

end
