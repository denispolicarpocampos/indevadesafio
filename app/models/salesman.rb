class Salesman < ApplicationRecord
  belongs_to :company
  has_many :goal_salesmen, dependent: :destroy
  has_many :goals, through: :goal_salesmen

  validates_uniqueness_of :name, scope: :company_id

  has_many :day_salesmen, dependent: :destroy
  has_many :days, through: :day_salesmen

end
