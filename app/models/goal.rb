class Goal < ApplicationRecord
  belongs_to :company
  has_many :days, dependent: :destroy
  has_many :goal_salesmen, dependent: :destroy
  has_many :salesmen, through: :goal_salesmen

  validates_presence_of :date_start, :date_end, :name, :totalvalue
  validates_uniqueness_of :date_start, :date_end, scope: :company_id
  validate :end_date_is_after_start_date

  after_save :create_days

  def create_days
    rangeDate = (self.date_start..self.date_end).to_a
    rangeDate.each do |date|
      self.days << Day.create(date_day: date, goal_id: self.id )
    end
  end

private

  def end_date_is_after_start_date
    if date_end < date_start
      errors.add(:date_end, "Data of end cannot be less than the data of start!")
    end
  end

end
