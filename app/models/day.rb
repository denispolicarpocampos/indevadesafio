class Day < ApplicationRecord
  belongs_to :goal
  has_many :day_salesmen, dependent: :destroy
  has_many :salesmen, through: :day_salesmen
  validates_presence_of :date_day, :goal_id

  accepts_nested_attributes_for :day_salesmen
end
