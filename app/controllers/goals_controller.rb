class GoalsController < ApplicationController
  before_action :find_goals, only: [:show, :edit, :update, :destroy]
  before_action :find_company, only: [:show, :new, :edit]

  def index
    @goal = current_owner.companies.find(params[:company_id]).goal
  end

  def new
    @goal = Goal.new
  end

  def show
    @goalcompanyid = @goal.company_id
    @date = Day.where(goal_id: params[:id]).pluck(:date_day, :id)
    @valueofalldays = @goal.days.where(goal_id: @goal.id).pluck(:value)
  end

  def create
    @goal = Goal.new(params_goal)
    if @goal.save
      @company = @goal.company
      flash[:notice] = "Goal created!"
      redirect_to company_path(:id => @company.id)
    else
      flash[:error] = "Could not create goal!"
      render :new
    end
  end

  def destroy
    @days = Day.find_by(goal_id: @goal.id)
    @days.destroy
    @goal.destroy
    @company = @goal.company
    flash[:notice] = "Goal deleted!"
    redirect_to company_path(:id => @company.id)
  end

  private

  def find_company
    @company = Company.find(params[:company_id])
  end

  def find_goals
    @goal = Goal.find(params[:id])
  end

  def params_goal
    params.require(:goal).permit(:name, :totalvalue, :date_start, :date_end, :month).merge(company_id: params[:company_id])
  end
end
