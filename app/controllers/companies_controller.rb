class CompaniesController < ApplicationController
  before_action :find_company, only: [:edit, :update, :destroy, :show]

  def index
    @company = current_owner.companies
  end

  def show
    authorize! :read, @company
  end

  def new
    @company = Company.new
  end

  def create
    @company = Company.new(params_company)
    if @company.save
      flash[:notice] = "Company saved!"
      redirect_to @company
    else
      flash[:error] = "Could not create company!"
      render :new
    end

  end

  def edit
    authorize! :update, @company
  end

  def update
    authorize! :update, @company
    if @company.update(params_company)
      flash[:notice] = "Company updated!"
      redirect_to @company
    else
      flash.now[:error] = "Could not update company!"
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @company
    @company.destroy
    flash[:notice] = "Company deleted!"
    redirect_to companies_path
  end

  private

  def find_company
    @company = Company.find(params[:id])
  end

  def params_company
    params.require(:company).permit(:name).merge(owner: current_owner)
  end
end
