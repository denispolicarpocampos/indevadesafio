class DaysController < ApplicationController
  before_action :find_day, only: [:show, :edit, :update]
  before_action :find_company, only: [:show, :edit]

  def index
    @day = current_owner.companies.find(params[:company_id]).goal.find(params[:goal_id]).days
  end

  def show
    @salesmen = @day.salesmen.pluck(:name)
    @salesmencount = @salesmen.count
    if @salesmencount > 0
      @valuesalesmen = @day.value.to_f/(@salesmencount)
    end
  end

  def edit
    @dayup = Day.new
    @day_salesmen = @dayup.day_salesmen.build
    @salesman = @day_salesmen.build_salesman
  end

  def update
    if @day.update(params_day)
      flash[:notice] = "Day updated!"
      redirect_to edit_company_goal_day_path(:goal_id => params[:goal_id], :id => params[:id])
    else
      flash[:error] = "Could not update day!"
      redirect_to edit_company_goal_day_path(:goal_id => params[:goal_id], :id => params[:id])
    end
  end

  private

  def find_company
    @company = Company.find(params[:company_id])
  end

  def find_day
    @day = Day.find(params[:id])
  end

  def params_day
    params.require(:day).permit(:value, salesman_attributes:[:id, :name]).merge(goal_id: params[:goal_id])
  end
end
