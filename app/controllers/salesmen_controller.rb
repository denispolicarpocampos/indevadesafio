class SalesmenController < ApplicationController
  before_action :find_salesman, only: [:edit, :update, :destroy]
  before_action :find_ids, only: [:create]

  def index
    @salesmen = current_owner.companies.find(params[:company_id]).salesman
    @company = Company.find(params[:company_id])
  end

  def create
    @salesman = Salesman.new(params_salesman)
    if @salesman.save
      DaySalesman.create(salesman_id: @salesman.id, day_id: @day_id)
      GoalSalesman.create(salesman_id: @salesman.id, goal_id: @goal_id)
      flash[:notice] = "Salesman saved!"
      redirect_to edit_company_goal_day_path(:goal_id => @goal_id, :id => @day_id)
    else
      @find_id = Salesman.find_by(name: @name).id
      @find_daysalesman = DaySalesman.find_by(salesman_id: @find_id, day_id: @day_id)
      unless @find_daysalesman
        DaySalesman.create(salesman_id: @find_id, day_id: @day_id)
        GoalSalesman.create(salesman_id: @find_id, goal_id: @goal_id)
        flash[:notice] = "Cannot create salesman because it already exist, but it was allocated on Day of Goal!"
        redirect_to edit_company_goal_day_path(:goal_id => @goal_id, :id => @day_id)
      else
        flash[:notice] = "You can not create a seller and can not join the Day of Goal because he is already associated!"
        redirect_to edit_company_goal_day_path(:goal_id => @goal_id, :id => @day_id)
      end
    end
  end

  def destroy
    @salesman.destroy
  end

  private

  def find_salesman
    @salesman = Salesman.find(params[:id])
  end

  def find_ids
    @day_id = params[:day][:day_id]
    @goal_id = params[:day][:goal_id]
    @name = params[:day][:salesman][:name]
  end

  def params_salesman
    params.require(:day).require(:salesman).permit(:name, :id).merge(company_id: params[:company_id])
  end
end
