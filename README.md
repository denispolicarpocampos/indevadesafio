# Desafio Indeva

Dependências: Docker, Docker-compose


Assim que clonar o projeto os seguintes comandos devem ser dados para rodar:

docker-compose build
docker-compose run --rm website bundle install
docker-compose run --rm website bundle exec rake db:create db:migrate
docker-compose up
